 alert('Hello World')

// console course
console.log('Hello World')
console.error('This is an error')
console.warn('Warning !');

// Variables
let score
score =10
console.log(score)

const date=17
console.log(date);

//Data Types
const nama = 'Elyakim';
const umur = 24;
const nilai = 4.0;

const x = null;
const y = undefined;
let z; 
console.log(typeof nama)
console.log(typeof umur)
console.log(typeof nilai)
console.log(typeof x)
console.log(typeof z)


// Strings

console.log('Hello, I am ' +nama+ ' and now, I am ' + umur)

console.log(`Nama saya ${nama} dan saya berusia ${umur} tahun`)

// String method

const a = 'I love you 3000'
console.log(a.substring(0, 6).toUpperCase())

console.log(a.toLowerCase())
console.log(a.split(''))

const negara = 'indonesia, malaysia, thailand, singapore'
console.log(negara.split(', '))

// Arrays
const numbers = [1,2,3,4,5]
const jajanan = ['bakso', 'soto', 'seblak', 'cilok']
console.log(numbers)
console.log(numbers[1])

jajanan[4] = 'batagor'
console.log(jajanan)

jajanan.push('sempol')

// insert first
jajanan.unshift('dimsum')

// Remove last
jajanan.pop()

// check
console.log(Array.isArray(jajanan))

// index 
console.log(jajanan.indexOf('bakso'))


// OBJECT LITERALS
const person = {
    namaDepan: 'Pak Rektor',
    usia: 45,
    hobi: ['writing', 'reading', 'sports'],
    alamat: {
      jalan: 'jalan mangga',
      desa: 'penari',
      kabupaten: 'canada'
    }
  }
  console.log(person)
  console.log(person.namaDepan)

// Array of todos
const todos = [
    {
      id: 1,
      text: 'Kuliah javascript',
      isComplete: true
    },
    {
      id: 2,
      text: 'Makan siang',
      isComplete: false
    },
    {
      id: 3,
      text: 'Progress report',
      isComplete: true
    }
  ]

  console.log(todos)
  console.log(todos[1].text)

// loop

// For
for(let i = 0; i <= 10; i++){
    console.log(`For Loop Number: ${i}`)
  }
  
  // While
  let i = 0
  while(i <= 10) {
    console.log(`While Loop Number: ${i}`)
    i++
  }

// loop in Array

for(let i = 0; i < todos.length; i++){
    console.log(` Todo ${i + 1}: ${todos[i].text}`)
  }

  for(let todo of todos) {
    console.log(todo.text)
  }

// High order Array

todos.forEach(function(todo, i, myTodos) {
    console.log(`${i + 1}: ${todo.text}`)
    console.log(myTodos)
    })
  
  const todoTextArray = todos.map(function(todo) {
    return todo.text
    })
  
  console.log(todoTextArray)
  
  const todo1 = todos.filter(function(todo) {
    return todo.id === 1
    })
  
// conditional
const t = 45
const u = 23

if(t == 10) {
  console.log('t is 45')
} else if(t > 10) {
  console.log('t is greater than 10')
} else {
  console.log('t is less than 10')
}

// if(t > 25 || u > 10) {
//     console.log('t is more than 25 or u is more than 10')
// })


// Switch
color = 'blue'

switch(color) {
  case 'red':
    console.log('color is red')
  case 'blue':
    console.log('color is blue')
  default:  
    console.log('color is not red or blue')
}

// Ternary operator / Shorthand if
const c = color === 'red' ? 10 : 20

// Function
// function addNums(num1 = 1, num2 = 1) {
//     console.log(num1 + num2)
// }
// addNums()
// addNums(6, 7)

const addNums = (num1 = 1, num2 = 1) => {
    return num1 + num2
}
console.log(addNums(5, 7))


// Constructor Function
function Person(firstName, lastName, dob) {
    this.firstName = firstName
    this.lastName = lastName
    this.dob = new Date(dob)
    // this.getBirthYear = function() {
    //     return this.dob.getFullYear()
    // }
    // this.getFullName = function() {
    //     return `${this.firstName} ${this.lastName}`
    // }
}

const person1 = new Person('Anang', 'Hermansyah', '1-4-1998')
const person2 = new Person('Ariel', 'Noah', '7-2-1990')

// console.log(person1.getBirthYear())
// console.log(person2.getFullYear())
Person.prototype.getFullName = function() {
    return `${this.firstName} ${this.lastName}`
    }
Person.prototype.getFullName = function() {
    return `${this.firstName} ${this.lastName}`
    }
 
console.log(person2);
  
  // console.log(person1.getBirthYear())
  // console.log(person1.getFullName())

// Selector

// console.log(window)
// alert(1)

// Single Element
// console.log(document.getElementById('my-form'))
// console.log(document.querySelector('.container')) 

// // Multiple Element 
console.log(document.querySelectorAll('.item'))
console.log(document.getElementsByTagName('li'))
console.log(document.getElementsByClassName('item'))

const items = document.querySelectorAll('.item')
items.forEach((item) => console.log(item))

// Manipulating DOM

const ul = document.querySelector('.items')
// ul.remove()
// ul.lastElementChild.remove()
// ul.firstElementChild.textContent = 'Welcome'
// ul.children[1].innerText = 'to the Jungle'
// ul.lastElementChild.innerHTML = '<h1>Everybody !</h1>';

const btn = document.querySelector('.btn')
btn.style.background = 'red'

// btn.addEventListener('mouseout', (e) => {
//     e.preventDefault()
//     // console.log('click')
//     document.getElementById('#my-form').style.background = '#ccc'
//     document.querySelector('body').classList.add('bg-dark')
//     document.querySelector('.items').lastElementChild.innerHTML = '<h1>Mantab</h1>'
// })

const myForm = document.querySelector('#my-form')
const nameInput = document.querySelector('#name')
const emailInput = document.querySelector('#email')
const msg = document.querySelector('.msg')
const userList = document.querySelector('#users')

myForm.addEventListener('submit', onSubmit)

function onSubmit(e) {
    e.preventDefault()
    if(nameInput.value === '' || emailInput.value === '') {
        msg.classList.add('error')
        msg.innerHTML = 'Please enter your name and email'
        // alert('Please enter your name')
        setTimeout(() => msg.remove(), 3000)
    }   else {
        // console.log('success')
        const li = document.createElement('li')
        li.appendChild(document.createTextNode
            (`${nameInput.value}: ${emailInput.value}`))
        
        userList.appendChild(li)
        
        // clear fields
        nameInput.value = ''
        emailInput.value = ''
    }
}
